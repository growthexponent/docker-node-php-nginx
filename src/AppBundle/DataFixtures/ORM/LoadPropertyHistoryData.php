<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PropertyBundle\Entity\PropertyHistory;

class LoadPropertyHistoryData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $propertyHistory = new PropertyHistory();
        $propertyHistory->setProperty($this->getReference('test-property'));
        $propertyHistory->setUser($this->getReference('admin-user'));

        $manager->persist($propertyHistory);
        $manager->flush();

        $this->addReference('test-property-history', $propertyHistory);
    }

    public function getOrder()
    {
        return 8;
    }
}