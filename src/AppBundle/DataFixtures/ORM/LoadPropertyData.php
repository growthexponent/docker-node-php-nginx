<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PropertyBundle\Entity\Property;

class LoadPropertyData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $property = new Property();
        $property->setTitle('Test Property Name');
        $property->setPhoto('testphoto.jpg');

        $property->setUnitNumber(72);
        $property->setStreetNumber(220);
        $property->setStreetName("Goulburn Street");
        $property->setLocality("Darlinghurst");
        $property->setPostalCode("2010");
        $property->setState("NSW");
        $property->setCountry("Australia");

        $manager->persist($property);
        $manager->flush();

        $this->addReference('test-property', $property);
    }

    public function getOrder()
    {
        return 5;
    }
}