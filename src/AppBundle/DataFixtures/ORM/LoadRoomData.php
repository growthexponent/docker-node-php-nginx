<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PropertyBundle\Entity\Room;

class LoadRoomData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $room = new Room();
        $room->setTitle('Test Room Name');
        $room->setPhoto('testphoto.jpg');
        $room->setProperty($this->getReference('test-property'));

        $manager->persist($room);
        $manager->flush();

        $this->addReference('test-room', $room);
    }

    public function getOrder()
    {
        return 6;
    }
}