<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use UserBundle\Entity\UserGroup;
use UserBundle\Entity\UserToken;

class LoadUserTokenData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userTokenAdmin = new UserToken();
        $userTokenAdmin->setUser($this->getReference('admin-user'));
        $userTokenAdmin->setToken('17c81225-8428-4957-a69d-9d43cc2372e8');

        $manager->persist($userTokenAdmin);
        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}