<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PropertyBundle\Entity\Content;

class LoadContentData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $content = new Content();
        $content->setRoom($this->getReference('test-room'));
        $content->setTitle('Test Content');
        $content->setValue(500);
        $content->setPhoto(["photo1.jpg", "photo2.jpg"]);
        $content->setReceipt('receipt.jpg');
        $content->setValueSource(["google" => 500, "ebay" => 400]);

        $manager->persist($content);
        $manager->flush();

        $this->addReference('test-content', $content);
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 7;
    }
}