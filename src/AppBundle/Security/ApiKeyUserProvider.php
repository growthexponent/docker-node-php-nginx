<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 13/04/17
 * Time: 6:44 PM
 */

namespace AppBundle\Security;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UserBundle\Repository\UserRepository;

class ApiKeyUserProvider implements UserProviderInterface
{

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $username
     * @return User
     */
    public function loadUserByUsername($username)
    {
        /** @var \UserBundle\Repository\UserRepository $repository */
        $repository = $this->em->getRepository('UserBundle:User');
        /** @var \UserBundle\Entity\User $existingUser */
        $existingUser = $repository->findOneBy(["username" => $username]);
        if (!empty($existingUser)) {
            return $existingUser;
        } else {
            throw new BadCredentialsException();
        }
    }

    /**
     * @param UserInterface $user
     * @return User
     */
    public function refreshUser(UserInterface $user)
    {
        /** @var \UserBundle\Repository\UserRepository $repository */
        $repository = $this->em->getRepository('UserBundle:User');
        /** @var \UserBundle\Entity\User $existingUser */
        $existingUser = $repository->findOneBy(["username" => $user->getUsername()]);
        if (!empty($existingUser)) {
            return $existingUser;
        } else {
            throw new BadCredentialsException();
        }
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return User::class === $class;
    }
}