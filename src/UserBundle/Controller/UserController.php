<?php

namespace UserBundle\Controller;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use UserBundle\Entity\User;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use UserBundle\Entity\UserToken;
use UserBundle\UserBundle;

class UserController extends FOSRestController
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        $authorizationChecker = $this->get('security.authorization_checker');

        // check for view access
        if (false === $authorizationChecker->isGranted('ROLE_USER_READ')) {
            throw new AccessDeniedException();
        }

        $tokenStorage = $this->get('security.token_storage');
        $currentUser = $tokenStorage->getToken()->getUser();

        $userToken = $this->getUserToken($currentUser);
        $currentUser = $currentUser->setToken($userToken->getToken());

        return $currentUser;
    }

    /**
     * @var User $user
     * @return UserToken
     */
    private function getUserToken($user)
    {
        $userToken = $this->getDoctrine()->getRepository('UserBundle:UserToken')->findOneByUser($user->getId());
        return $userToken;
    }

    /**
     * @return array|View
     */
    public function listAction()
    {
        $authorizationChecker = $this->get('security.authorization_checker');

        // check for view access
        if (false === $authorizationChecker->isGranted('ROLE_USER_LIST')) {
            throw new AccessDeniedException();
        }

        $restResult = $this->getDoctrine()->getRepository('UserBundle:User')->findAll();

        if ($restResult === null) {
            return new View("No users found", Response::HTTP_OK);
        }
        return $restResult;
    }

    /**
     * @param $id
     * @return View|null|object
     */
    public function getAction($id)
    {
        $authorizationChecker = $this->get('security.authorization_checker');

        // check for view access
        if (false === $authorizationChecker->isGranted('ROLE_USER_READ')) {
            throw new AccessDeniedException();
        }

        $singleResult = $this->getDoctrine()->getRepository('UserBundle:User')->find($id);
        if ($singleResult === null) {
            return new View("User not found", Response::HTTP_OK);
        }

        $tokenStorage = $this->get('security.token_storage');
        $currentUser = $tokenStorage->getToken()->getUser();

        if ($singleResult->getId() != $currentUser->getId()) {
            // check for view access
            if (false === $authorizationChecker->isGranted('VIEW', $singleResult)) {
                throw new AccessDeniedException();
            }
        }

        return $singleResult;
    }

    /**
     * @param $json
     * @return View
     */
    public function addAction($json)
    {
        $authorizationChecker = $this->get('security.authorization_checker');

        // check for view access
        if (false === $authorizationChecker->isGranted('ROLE_USER_CREATE')) {
            throw new AccessDeniedException();
        }

        $serializer = $this->get('serializer');
        $users = $serializer->deserialize($json, 'array<UserBundle\Entity\User>', 'json');
        $returnMessage = "Add user failed";
        $okResponse = false;

        $em = $this->getDoctrine()->getManager();
        foreach ($users as $newUser) {
            if ($newUser->getId() != null) {
                // Update
                $existingUser = $this->getDoctrine()->getRepository('UserBundle:User')->find($newUser->getId());
                $returnMessage = "User already exists with that id";
                $okResponse = true;

            } else {
                // Insert
                $existingEmailAddress = $this->getDoctrine()->getRepository('UserBundle:User')->findOneByEmail($newUser->getEmail());
                if (!empty($existingEmailAddress)) {
                    $returnMessage = "Email address already used";
                    $okResponse = true;
                }
                $existingMobile = $this->getDoctrine()->getRepository('UserBundle:User')->findOneByMobile($newUser->getMobile());
                if (!empty($existingMobile)) {
                    $returnMessage = "Mobile number already used";
                    $okResponse = true;
                }

                $newUser->setLastModifiedAt(date('Y-m-d H:i:s'));
                $em->persist($newUser);
                $em->flush();

                $returnMessage = "User Added Successfully";
                $okResponse = true;
            }

            // creating the ACL
            $aclProvider = $this->get('security.acl.provider');
            $objectIdentity = ObjectIdentity::fromDomainObject($newUser);
            $acl = $aclProvider->createAcl($objectIdentity);

            // retrieving the security identity of the currently logged-in user
            $tokenStorage = $this->get('security.token_storage');
            $user = $tokenStorage->getToken()->getUser();
            $securityIdentity = UserSecurityIdentity::fromAccount($user);

            // grant owner access
            $acl->insertObjectAcl($securityIdentity, MaskBuilder::MASK_OWNER);
            $aclProvider->updateAcl($acl);

        }
        return new View($returnMessage, Response::HTTP_OK);
    }

    /**
     * @param $json
     * @return View
     */
    public function editAction($json)
    {
        $authorizationChecker = $this->get('security.authorization_checker');

        // check for view access
        if (false === $authorizationChecker->isGranted('ROLE_USER_UPDATE')) {
            throw new AccessDeniedException();
        }

        $serializer = $this->get('serializer');
        $users = $serializer->deserialize($json, 'array<UserBundle\Entity\User>', 'json');
        $returnMessage = "Edit user failed";
        $okResponse = false;

        $em = $this->getDoctrine()->getManager();
        foreach ($users as $user) {
            if ($user->getId() != null) {
                // Update

                $tokenStorage = $this->get('security.token_storage');
                $currentUser = $tokenStorage->getToken()->getUser();

                if ($user->getId() != $currentUser->getId()) {
                    $authorizationChecker = $this->get('security.authorization_checker');

                    // check for edit access
                    if (false === $authorizationChecker->isGranted('EDIT', $user)) {
                        throw new AccessDeniedException();
                    }
                }

                $existingUser = $this->getDoctrine()->getRepository('UserBundle:User')->find($user->getId());
                if (!empty($existingUser)) {
                    $existingUser->setEmail($user->getEmail());
                    $existingUser->setPassword($user->getPassword());
                    $existingUser->setFirstName($user->getFirstName());
                    $existingUser->setMiddleName($user->getMiddleName());
                    $existingUser->setLastName($user->getLastName());
                    $existingUser->setMobile($user->getMobile());
                    $existingUser->setLastModifiedAt(date('Y-m-d H:i:s'));
                    $em->flush();

                    $returnMessage = "User updated successfully";
                    $okResponse = true;
                }
            }
        }
        return new View($returnMessage, Response::HTTP_OK);
    }
}
