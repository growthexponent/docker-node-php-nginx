<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/', [], [], ["AuthorizationBasic"=>"xxx"]);
        $this->assertContains('Hello World', $client->getResponse()->getContent());
    }
}
