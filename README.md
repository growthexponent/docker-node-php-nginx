**Checkout repo**

There are issues in the symfony files as it was copied from a different project so these can be deleted if not relevant to you.
The purpose of this repo is the basic docker setup for bootstrapping new projects.
Docker containers started in this project are:
_mysql, php, nginx, elk, redis and nodejs_

Docker-compose version 3.3 and latest versions of images and packages when created.

**Start containers**
`docker-compose up --build`

**Check Containers**

Add host entry to access your servers.
Usually 192.168.99.100

PHPInfo
http://php-docker.local/index.php

Kibana
http://php-docker.local:81

MySql is running on 3306

**Run Composer Update**
`docker-compose exec php composer update`

**Database Migrations (Laravel, Drupal)**
`docker-compose exec web php artisan migrate`

**Run Tests**
`docker-compose -p tests run --rm web phpunit`